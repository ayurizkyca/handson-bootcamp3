import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<Date, String> formatDate = someDate -> {
            SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
            return sdf.format(someDate);
        };

        // Contoh penggunaan
        Date currentDate = new Date();
        String formattedDate = formatDate.apply(currentDate);
        System.out.println("Formatted Date: " + formattedDate);
    }
}



